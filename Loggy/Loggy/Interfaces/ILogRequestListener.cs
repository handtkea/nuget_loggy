﻿using System;
using System.Collections.Generic;
using System.Text;
using static LoggyLib.Program;

namespace LoggyLib.Interfaces
{
    public interface ILogRequestListener
    {

        // Fields
        long ListenerId { get; set; }

        // Methods
        void Log(string message, string method, bool isError, bool insertBlankLineBeforeLine, long listenerId);

    }

}
