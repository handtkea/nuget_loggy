﻿using LoggyLib.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using static LoggyLib.Loggy;
using static LoggyLib.Program;

namespace LoggyLib.LogRequestListener
{

    public class FlatFileLogRequestListener : ILogRequestListener
    {

        // Private Members.
        private string mFlatFileFullPath;
        private RequestListenerType mLogListenerType;

        // Public fields.
        public long ListenerId { get; set; }

        // Constructors.
        public FlatFileLogRequestListener(string param)
        {

            // Init members.
            mFlatFileFullPath = param;
            mLogListenerType = RequestListenerType.FlatFile;
            ListenerId = GenerateListenerId();

        }


        /// <summary>
        /// Writes given message to flat log file.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="method"></param>
        /// <param name="isError"></param>
        /// <param name="insertBlankLineBeforeLine"></param>
        /// <param name="listenerId"></param>
        public void Log(string message, string method, bool isError, bool insertBlankLineBeforeLine, long listenerId)
        {

            try
            {

                if (string.IsNullOrWhiteSpace(message))
                {
                    throw new Exception("Empty log message.");
                }

                using (StreamWriter sw = File.AppendText(mFlatFileFullPath))
                {

                    if (insertBlankLineBeforeLine)
                    {
                        sw.WriteLine("");
                    }

                    string tmpMessageType = string.Empty;

                    if (isError)
                    {
                        tmpMessageType = " > Error > ";
                    }
                    else
                    {
                        tmpMessageType = " > Info  > ";
                    }

                    if (!string.IsNullOrWhiteSpace(method))
                    {
                        sw.WriteLine(DateTime.Now + tmpMessageType + method + " > " + message);
                    }
                    else
                    {
                        sw.WriteLine(DateTime.Now + tmpMessageType + message);
                    }

                }

            }
            catch (Exception ex)
            {

            }

        }

        private long GenerateListenerId()
        {

            return 123456;
        }



    }

}
